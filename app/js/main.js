$(document).ready(function() {
	svg4everybody();
	// $('.phone').inputmask({"mask": "8 (999) 999-99-99"});
	// (function() {
	// 	'use strict';
	// 	window.addEventListener('load', function() {
	// 	var forms = document.getElementsByClassName('needs-validation');
	// 	var validation = Array.prototype.filter.call(forms, function(form) {
	// 		form.addEventListener('submit', function(event) {
	// 			if (form.checkValidity() === false) {
	// 				event.preventDefault();
	// 				event.stopPropagation();
	// 			} else {
	// 				event.preventDefault();
	// 				$.ajax({
	// 					url: "action.php",
	// 					type: "post",
	// 					data: $(form).serialize(),
	// 					success: function(e) {
	// 						$(form).removeClass('was-validated');
	// 						$(form).find('.form-control').val('');
	// 						$('#thanksModal').modal('show')
	// 					}
	// 				})
	// 			}
	// 			form.classList.add('was-validated');
	// 		}, false);
	// 	});
	// }, false);
	// })();
	$('[data-toggle]').click(function() {
		if ( $(this).data('toggle') == 'toggleVisible' ) {
			var target = $(this).data('target');
			$('[data-id="'+target+'"]').toggleClass('visible');
			console.log(target)
			if ( target == 'mobile-collapse' ) {
				$('body').toggleClass('overflow-hidden bg-mobile');
				$('.wrapper-content').toggleClass('d-none')
			}
			if ( target == 'notif' ) {
				$('.categorys').toggleClass('unvisible')
			}
		}
		if ( $(this).data('toggle') == 'anchor' ) {
			var offsetHeight = $('.header').outerHeight();
			var target = $($(this).attr('href')).offset().top - (offsetHeight - 1);
			$('body,html').animate({
				scrollTop: target
			}, 500);
		}
	});

	function checkLazyManyItems(e) {
		var slides = e.relatedTarget.$stage.children();
		var allItemsIndex = e.item.count;
		
		var activeElems = 0;
		[].forEach.call(slides, function(elem) {
			if ( elem.classList.contains('active') ) {
				activeElems += 1;
			}
		})
		
		if ( activeElems >= allItemsIndex ) {
			$(e.target).find('.owl-nav').hide()
		} else {
			$(e.target).find('.owl-nav').show()
		}
	};

	function setSliderGameMenu() {
		$('.menu-carousel').owlCarousel({
			autoWidth: true,
			nav: true,
			dots: false,
			onInitialized: function(e) {
				checkLazyManyItems(e);
			},
			onResized: function(e) {
				checkLazyManyItems(e);
			},
			onTranslated: function(e) {
				checkLazyManyItems(e);
			}
		})
	}

	function changePosition() {
		if ( $(window).width() < 974 ) {
			$('.notifications-drop').appendTo('body');
			$('.lang').prependTo('.sidebar-in');
			$('.categorys').appendTo('body');
			$('.menu-carousel').owlCarousel('destroy');
		} else {
			$('body > .notifications-drop').appendTo('.notifications');
			$('.sidebar-in .lang').appendTo('.header-logo-wrap > div')
			$('body .categorys').prependTo('.header-menu');
			setSliderGameMenu();
		}
	}
	changePosition()
	$(window).resize(function() {
		changePosition()
	})
	$('.payment-carousel').owlCarousel({
		autoWidth: true,
		nav: true,
		dots: false,
		onInitialized: function(e) {
			checkLazyManyItems(e);
		},
		onResized: function(e) {
			checkLazyManyItems(e);
		},
		onTranslated: function(e) {
			checkLazyManyItems(e);
		}
	});
	$('.ready-sets-carousel').owlCarousel({
		// autoWidth: true,
		items: 6,
		nav: true,
		dots: false,
		// margin: 54,
		responsive: {
			0: {
				items: 1,
			},
			400: {
				items: 2,
			},
			576: {
				items: 3,
			},
			768: {
				items: 4,
			},
			992: {
				items: 3,
			},
			1200: {
				items: 6,
				margin: 54,
				autoWidth: true
			}
		},
		onInitialized: function(e) {
			checkLazyManyItems(e);
		},
		onResized: function(e) {
			checkLazyManyItems(e);
		},
		onTranslated: function(e) {
			checkLazyManyItems(e);
		}
	})

	// $('.search-input input').focus(function() {
	// 	$(this).focus()
	// });

	// var contactFlag = false;
	// if ( $(window).width() < 1183 ) {
	// 	if ( !contactFlag ) {
	// 		$('.header__contacts').appendTo('.menu-inner')
	// 		$('.wow').removeClass('wow')
	// 		contactFlag = true;
	// 	}
	// } else {
	// 	if ( contactFlag ) {
	// 		$('.header__contacts').insertBefore('.header__button-wrap')
	// 		contactFlag = false
	// 	}
	// }
	// $(window).resize(function() {
	// 	offsetHeight = $('.header').height();
	// 	if ( $(window).width() < 1183 ) {
	// 		if ( !contactFlag ) {
	// 			$('.header__contacts').appendTo('.menu-inner')
	// 			$('.wow').removeClass('wow')
	// 			contactFlag = true;
	// 		}
	// 	} else {
	// 		if ( contactFlag ) {
	// 			$('.header__contacts').insertBefore('.header__button-wrap')
	// 			contactFlag = false
	// 		}
	// 	}
	// })
	// $(window).scroll(function() {
	// 	var pageTop = $(this).scrollTop();
	// 	if ( pageTop > 10 ) {
	// 		$('.header').addClass('sticky')
	// 	} else {
	// 		$('.header').removeClass('sticky')
	// 	}
	// })

	// $('.checkbox-input').change(function() {
	// 	if ( !$(this).prop('checked') ) {
	// 		$(this).closest('form').find('button[type="submit"]').attr('disabled', true)
	// 	} else {
	// 		$(this).closest('form').find('button[type="submit"]').removeAttr('disabled')
	// 	}
	// })


	// var offsetHeight = $('.header').outerHeight() + 10;
	// $('body').scrollspy({
	// 	offset: offsetHeight
	// });

	// $('.menu-nav li a').click(function (event) {
	// 	var offsetHeight = $('.header').outerHeight();
	// 	var scrollPos = $($(this).attr('href')).offset().top - (offsetHeight - 1);
	// 	$('body,html').animate({
	// 		scrollTop: scrollPos
	// 	}, 500);
	// 	if ( $(window).width() < 1520 ) {
	// 		$('.menu-close').click()
	// 	}
	// 	return false;
	// });

	// $('[data-click="animated"]').click(function() {
	// 	var $this = $(this);
	// 	var duration = $(this).data('duration');
	// 	$(this).append('<span class="button-click"></span>')
	// 	setTimeout(function() {
	// 		$this.children('.button-click').remove();
	// 	}, duration)
	// })



	// new WOW().init({
	// 	mobile: false,
	// 	resetAnimation: false,
	// });

	// $(window).scroll(function() {
	// 	var pageTop = $(window).scrollTop();
	// 	if ( pageTop > $('.header').outerHeight() ) {
	// 		$('.sidebar').css({'padding-top': $('.header').outerHeight()})
	// 	} else {
	// 		$('.sidebar').css({'padding-top': 0})
	// 	}
	// })
})
